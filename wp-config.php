<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+~q4SJ#YI+ PL;(Ov?nT+lSG+KZYX!f|6(M#I1G|OA!hb$(b5Dpd+G? ^ NI#PEB' );
define( 'SECURE_AUTH_KEY',  '0c[ATHP>2HwN^}5}1eW_~e}FQkAE:WzB/+K|x!n@%-0Q!a<PT|J9BM#-V=,wbR,Y' );
define( 'LOGGED_IN_KEY',    'n9c<U@}(g}0=W>:/7S{zLt;Xpqn{|mRH4uaojj-5qP<-kgy{$p<}D#{Zx+p*L !a' );
define( 'NONCE_KEY',        ']*.fQe1[yc x6CZseSkKS.mLZH=OU@L5pJMA5Ea|<d#K 1w~Jf0Ly9ME=g=V>{P4' );
define( 'AUTH_SALT',        '@+ZsTW+`/#(+>unWRujze&4E_}L9o2&7y;0d*uCn3plmY1$MIrX_paVxM;DE|21T' );
define( 'SECURE_AUTH_SALT', 'eD&yw6PtXXBqa60%w=@`:asVP#4$&<P)*6Jz@C)(_?9B[87+>>6CkW-RVxh>a<:}' );
define( 'LOGGED_IN_SALT',   '6yf)gDZ8>Zb|dH$~wIFWpR;+9Ow.&5*!C-zlVLnULB$AJBp/iWp1m=$G@12^|y=q' );
define( 'NONCE_SALT',       '-5!>HQD]E/_b:K.vDJle+o7V7r?xx8>v}jbi:R8j`l:7KhoY5Xfbuki:K=38ga^&' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
